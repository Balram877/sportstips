# CGTipster #

CGTipster mobile app is built using:

* [Node.js](https://nodejs.org/en/)
* [Apache Cordova](https://cordova.apache.org/) 
* [Ionic Framework](http://ionicframework.com/) 
* [AngularJS](https://angularjs.org/)

## Notes
1. To test UI of locked items without worrying about "buying",
`uncomment` ```if(true)``` and `comment` the actual if condition in ```checkPurchase``` method in *mainCtrl.js*
2. I have placed the Certificate Signing Request (CSR) of my Macbook Air inside `ios-certificates` forlder in the root of this project. I might need it to renew old certificates or to create new ones


## Screen sizes for Appstor screenshots
For more info [go here](http://iosres.com/)

| Display (inch) | iOS Device |
|:--------:|:---------|
| 5.5 | iPhone 7 Plus |
| 4.7 | iPhone 7 |
| 4 | iPhone 5 |
| 3.5 | iPhone 4s |
| 12.9 | iPad Pro 12.9 |
| 9.7 | iPad Pro 9.7 |

# Requirements #

## Initial Setup ##

1. Download and install [Node.js](https://nodejs.org/en/)
2. Download and install [Git](https://git-scm.com/downloads)
3. Open Command Prompt (Windows) or Terminal (Mac)
4. Install cordova using ``` npm install cordova -g ```
5. Install ionic using ``` npm install ionic -g ```

### <a name="nvmhead"></a> Node version ###
* Install [Homebrew](http://brew.sh).
* Install NVM (Node Version Manager) using

```
brew install nvm
```

* Follow the nvm setup instructions output after NVM installation on the terminal window

* Switch over to using NodeJS `v7.4.0`

```
nvm use v7.4.0
```

**If the above command gives the error** `N/A: version "v7.4.0" is not yet installed`, then install NodeJS version v7.4.0 via nvm using
```
nvm install v7.4.0
```

and then switch to v7.4.0 via nvm using 
```
nvm use v7.4.0
```

## Project Setup ##

1. Clone this repository
2.  ``` cd ``` into this repository 
3. nvm use v7.4.0
4.  `npm install`
5.  `ionic hooks add`
6. `chmod +x hooks/after_prepare/010_add_platform_class.js`

## Android Setup ##

1. Download and install [JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
2. Download and install [Android SDK](https://developer.android.com/studio/index.html)
3. After Android SDK has been installed, launch SDK Manager. Use SDK Manger to follow the next step.
4. Install the following packages:
    * SDK Platform (API 24, at the time of writing this) 
    * Android SDK Platform-tools (latest Rev.) 
    * Android SDK Build-tools (latest Rev.) 
 
## iOS Setup ##

1. Install [XCode](https://itunes.apple.com/en/app/xcode/id497799835?mt=12)

* ### **Build for Browser** ###
1. To preview the app on browser, run ``` ionic serve -l ```
**NOTE: Browser will show a bunch of errors and won't run as expected because the cordova plugins cannot run on browser and will throw exceptions. You must build for Android and iOS for proper use**

* ### **Build for Android** ###
1. ```cordova platform add android --verbose```
2. ```cordova prepare``` (not needed if all plugins have been added into the platform already via cordova platform add android)
3. ```ionic run android```

* ### **Build for iOS** ###
1. ```cordova platform add android --verbose```
2. ```cordova prepare``` (not needed if all plugins have been added into the platform already via cordova platform add android)
3. ```ionic run android```

### A note on ```cordova prepare``` ###
The command ```cordova prepare``` takes all plugin declarations from `config.xml` and installs them.

### Why should I avoid **ionic state restore** ###
Ionic state restore crashes at some plugins; crashes because plugins don't get their variables supplied in command line; fetches plugins from package.json (which does not have variables, version numbers etc). Whereas **cordova prepare** fetches plugins from **config.xml** like "Phonegap Build".

### Common build failure fixes ###

  * #### Android #####
1. If `ionic platform add android` fails at "BILLING_KEY missing", then even though the platform has been added, the plugins have not been installed for android. You'll need to manually install all plugins using `ionic plugin add <plugin name>` one-by-one from `config.xml`
2. See if you need to update any packages in Android SDK Manager (path on Mac: ~/Library/Android/sdk/tools/android)

  * #### iOS #####
1. To run on device, do not forget the --device flag
2. You need to run it first from within XCode before it can run on your device via ionic CLI.

  * #### Other ####
  1. Make sure you are using node [v7.4.0 ](#nvmhead)

### Helpful Links ###
1. [Splash and Icon Sizes](https://github.com/phonegap/phonegap/wiki/App-Icon-Sizes)


### Note
* **29 July 2017** - Commit just to get done with it. Not sure if project setup perfectly works after cloning. Should work, but not sure. In case of any problems, the iOS version repo might help as that one was perfected.
