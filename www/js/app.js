var app = angular.module('tipster', ['ionic', 'ngCordova', 'pascalprecht.translate'])

app.run(function ($ionicPlatform, $rootScope, $translate, $ionicLoading) {
    $ionicPlatform.ready(function () {
        $rootScope.devicePlatform = device.platform;
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            if ($rootScope.devicePlatform == 'iOS') {
                StatusBar.hide();
            } else {
                StatusBar.styleDefault();
            }
        }
        window.FirebasePlugin.grantPermission();

        navigator.globalization.getPreferredLanguage(
            function (language) {
                $translate.use(language.value)
            },
            function () {
                $translate.use("en-GB")
            }
        );
    });


})

app.config(function ($stateProvider, $urlRouterProvider, $translateProvider) {

    $stateProvider
        .state('main', {
            url: '/main',
            templateUrl: 'templates/main.html',
            controller: 'MainCtrl'
        })

    $urlRouterProvider.otherwise('/main');

    $translateProvider.useStaticFilesLoader({
        prefix: 'languages/',
        suffix: '.json'
    });


    $translateProvider

        .registerAvailableLanguageKeys(['en-GB', 'en-AU', 'en-US', 'de', 'el', 'es', 'fr', 'id', 'it', 'ja', 'ko', 'pt', 'ro', 'ru', 'sv', 'sw', 'vi', 'zh'], {
            'en-GB': 'en-GB',
            'en-AU': 'en-AU',
            'en-US': 'en-US',
            'en-*': 'en-GB',
            'de': 'de',
            'de-*': 'de',
            'el': 'el',
            'el-*': 'el',
            'es': 'es',
            'es-*': 'es',
            'fr': 'fr',
            'fr-*': 'fr',
            'id': 'id',
            'id-*': 'id',
            'it': 'it',
            'it-*': 'it',
            'ja': 'ja',
            'ja-*': 'ja',
            'ko': 'ko',
            'ko-*': 'ko',
            'pt': 'pt',
            'pt-*': 'pt',
            'ro': 'ro',
            'ro-*': 'ro',
            'ru': 'ru',
            'ru-*': 'ru',
            'sv': 'sv',
            'sv-*': 'sv',
            'sw': 'sw',
            'sw-*': 'sw',
            'vi': 'vi',
            'vi-*': 'vi',
            'zh': 'zh',
            'zh-*': 'zh',
            '*': 'en-GB'
        })

    $translateProvider

        .fallbackLanguage('en-GB');


});

app.directive('landing', function () {
    return {
        restrict: 'E',
        templateUrl: 'landing/landing.html'
    }
})

app.directive('mySideMenuContent', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/my-side-menu-content.html'
    }
})

app.directive('myTabsContent', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/my-tabs-content.html'
    }
})

app.directive('chooseLeague', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/choose-league.html'
    }
})

app.directive('chooseLeagueStats', function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/choose-league-stats.html'
    }
})

app.filter('TitleFilter', function () {
    return function (items) {
        var result = {};
        angular.forEach(items, function (value, key) {
            if (value === true) {
                result[key] = value;
            }
        });
        return result;
    };
});

app.filter('myOrderBy', function () {
    return function (items, field) {
        var filtered = [];
        angular.forEach(items, function (item) {
            filtered.push(item);
        });
        return filtered;
    };
});

app.filter('localDate', function () {
    return function (x) {

        var tz = moment.tz.guess();
        var date = moment.utc(x).tz(tz).format('YYYY-MM-DD HH:mm');

        return date;
    }
});
