app.controller('MainCtrl', function ($scope, $filter, $timeout, $ionicScrollDelegate, $rootScope, $cordovaToast, $cordovaNetwork, $ionicPlatform, $cordovaDialogs, $cordovaNetwork, $filter, $ionicSideMenuDelegate, $ionicLoading, $http, $cordovaSocialSharing, $ionicPopup, endPointGeneratorService, admobService, inAppPurchaseService, apiService, $ionicScrollDelegate, $q) {

    var onlineOnStart = true;

    $scope.nonFootballTips = [];
    $scope.footballTips = {};

    $scope.winType_list = [];

    $scope.datesList = [];

    $scope.loading = {
        football: false
    }

    var scrollableDate_centerIndex = 0;
    var defaultNotificationText = 'Tap to view tips for today';
    $scope.delay5starPurchaseBtn = false;
    $scope.isPurchasedGold = false;
    $scope.isStarted = true;

    var restoredPurchase = {
        restoreClicked: false,
        restored: false
    }
    $scope.notifyTime = {
        'time': new Date(),
        'disable': false,
        'schedule': new Date()
    }

    var notificationEnabled = true
    var popUp = null;
    var wifiAlert = {
        'message': 'No Internet Connection',
        'title': 'Network Error',
        'button': 'OK'
    }

    var itemsInStoreCount = 0;
    var purchaseCounter = 0;
    $scope.newsFeed = '';
    $scope.showNewsFeed = false;
    $scope.stats = [];
    $scope.noFilterTips = [];
    $scope.internet = true;
    $scope.chooseLeagueStatsBool = false;
    $scope.leagueSearch = { value: '' }
    $scope.statsLeague = ''
    $scope.statsLeagueHeader = '';

    $scope.popType = ''
    $scope.badges = "";
    $scope.urlArray = null;
    $scope.content = [];

    $scope.leagues = [];
    $scope.allLeaguesArray = [];
    $scope.leagueValues = {
        'trueValue': 0,
        'falseValue': 0
    }
    $scope.selectAllLeagues = false;
    $scope.selectedLeagues = [];

    $scope.filteredResult = [];

    $scope.sideMenuOptions = {
        'Football Tips': true,
        'Horse Racing Tips': false,
        'NFL Tips': false,
        'Accuracy': false,
        'Settings': false,
        'Choose League': false,
        'Rate Us': false
    }

    $scope.urlArgs = {
        first: 'Football Tips',
        star: '4 Star',
        winType: 'Home Wins',
        statsType: 'Main Table'
    }
///////////////////////////////////////////////////////////////////////
$scope.shouldUpdate = function () {
       
    
        if ($scope.devicePlatform == 'iOS') {
            // iOS
            window.open("https://itunes.apple.com/us/app/cgtipster-sports-tips-stats/id1019083868?mt=8","_system");
        } else {
            window.open("http://play.google.com/store/apps/details?id=com.bigedev.cgtipster&hl=en","_system");
        }

       
    }
/////////////////////////////////////////////////////////////////////////
    $scope.accuracyorder = ["FiveStarHome", "FourStarHome",
        "FiveStarAway", "FourStarAway", "FiveStarHigh", "FourStarHigh", "FiveStarOver2", "FourStarOver2", "FiveStarLow", "FourStarLow", "FiveStarBTTS", "FourStarBTTS"]

    $scope.statsOrder = ['Pld', 'P', 'W', 'D', 'L', 'GF', 'GA', 'GD']

    var purchaseItem;
    $scope.purchase = {
        '5startips': {
            value: false,
            dirtyBit: false
        },
        'Horse Racing Tips': {
            value: false,
            dirtyBit: false
        },
        'NFL Tips': {
            value: false,
            dirtyBit: false
        },
        'Boxing Tips': {
            value: false,
            dirtyBit: false
        },
        'Basketball Tips': {
            value: false,
            dirtyBit: false
        },
        'Baseball Tips': {
            value: false,
            dirtyBit: false
        },
        'NHL Tips': {
            value: false,
            dirtyBit: false
        },
        'Golf Tips': {
            value: false,
            dirtyBit: false
        },
        'Cricket Tips': {
            value: false,
            dirtyBit: false
        },
        'Tennis Tips': {
            value: false,
            dirtyBit: false
        },
        'Motor Racing Tips': {
            value: false,
            dirtyBit: false
        },
        'entireapp': {
            value: false,
            dirtyBit: false
        }
    }

    $scope.accuracy = {
        'FiveStarHome': {
            name: '5 Star Home Wins',
            percent: '100'
        },
        'FourStarHome': {
            name: '4 Star Home Wins',
            percent: '100'
        },
        'FiveStarAway': {
            name: '5 Star Away Wins',
            percent: '100'
        },
        'FourStarAway': {
            name: '4 Star Away Wins',
            percent: '100'
        },
        'FiveStarHigh': {
            name: '5 Star Over 1.5',
            percent: '100'
        },
        'FourStarHigh': {
            name: '4 Star Over 1.5',
            percent: '100'
        },
        'FiveStarOver2': {
            name: '5 Star Over 2.5',
            percent: '100'
        },
        'FourStarOver2': {
            name: '4 Star Over 2.5',
            percent: '100'
        },
        'FiveStarLow': {
            name: '5 Star Under 2.5',
            percent: '100'
        },
        'FourStarLow': {
            name: '4 Star Under 2.5',
            percent: '100'
        },
        'FiveStarBTTS': {
            name: '5 Star BTTS',
            percent: '100'
        },
        'FourStarBTTS': {
            name: '4 Star BTTS',
            percent: '100'
        }
    }

    removeAd = function () {
        if (
            $scope.purchase['5startips'].value == true &&
            $scope.purchase['Horse Racing Tips'].value == true &&
            $scope.purchase['NFL Tips'].value == true &&
            $scope.purchase['Boxing Tips'].value == true &&
            $scope.purchase['Cricket Tips'].value == true &&
            $scope.purchase['Tennis Tips'].value == true &&
            $scope.purchase['NHL Tips'].value == true &&
            $scope.purchase['Basketball Tips'].value == true &&
            $scope.purchase['Baseball Tips'].value == true &&
            $scope.purchase['Motor Racing Tips'].value == true &&
            $scope.purchase['Golf Tips'].value == true) {
            admobService.removeAds();
        }

    }

    var setScrollableDatesScroll = function () {
        $scope.scrollableDate_center = datesList[scrollableDate_centerIndex];
        if (scrollableDate_centerIndex == 0) {
            $scope.scrollableDate_left = "";
        } else {
            $scope.scrollableDate_left = datesList[scrollableDate_centerIndex - 1]
        }
        if (scrollableDate_centerIndex == datesList.length - 1) {
            $scope.scrollableDate_right = "";
        } else {
            $scope.scrollableDate_right = datesList[scrollableDate_centerIndex + 1]
        }
    }

    var initialize = function () { // will run every time the app is initialized

        $scope.winType_list = endPointGeneratorService.getAllWinTypes();

        itemsInStoreCount = Object.keys($scope.purchase).length; // to count number of properties of object purchase
        if ($scope.badges == "") {

            getBadges();
            getLeagues();

            if (window.store) {

                inAppPurchaseService.initializeStore();

                store.products.forEach(function (product) {

                    store.when(product.alias).cancelled(function (p) {
                        $scope.purchase[p.alias].dirtyBit = false;
                    });

                    store.when(product.alias).error(function (p) {
                        $scope.purchase[p.alias].dirtyBit = false;
                    });

                    store.when(product.alias).approved(function (p) {
                        p.finish()
                    });

                    if (product.alias == "5startips") registerOwnedEventHandlerFor5Star();
                    else if (product.alias == "entireapp") registerOwnedEventHandlerForEntireApp();
                    else {

                        store.when(product.alias).owned(function (p) {
                            $scope.purchase[p.alias].value = true;
                            if ($scope.purchase.entireapp.value != true && $scope.purchase[p.alias].dirtyBit == true) {
                                $scope.changeMenu(p.alias);
                                $scope.getJson();
                            }

                            if (++purchaseCounter >= itemsInStoreCount - 1) {
                                $scope.showNewsFeed = true;

                                // removeAd();
                            }
                        });

                    }

                }, this);

                function registerOwnedEventHandlerFor5Star() {
                    store.when("5startips").owned(function (p) {
                        $scope.purchase['5startips'].value = true;
                        $scope.delay5starPurchaseBtn = false;
                        $scope.isPurchasedGold = true;
                        $timeout(function () {
                            $scope.$apply();
                        })

                        if (++purchaseCounter >= itemsInStoreCount - 1) {
                            $scope.showNewsFeed = true;
                            // removeAd();

                        }
                    });
                }

                function registerOwnedEventHandlerForEntireApp() {
                    store.when("entireapp").owned(function (p) {
                        $scope.purchase.entireapp.value = true;
                        for (var property in $scope.purchase) {
                            if ($scope.purchase.hasOwnProperty(property)) {
                                $scope.purchase[property].value = true;
                            }
                        }
                        $scope.delay5starPurchaseBtn = false;
                        $scope.isPurchasedGold = true;
                        $timeout(function () {
                            $scope.$apply();
                            //$digest or $apply
                        })
                        if ($scope.purchase['entireapp'].dirtyBit == true) {
                            $scope.changeMenu('Football Tips');
                            $scope.getJson();
                        }

                        $scope.showNewsFeed = true;
                        removeAd();
                    });
                }

                ////////////////////////////////////////////////

                $scope.startPurchase = function (item) {
                    if (item != '5startips') {
                        toggleLeft(false);
                    }
                    if ($scope.purchase[item].value != true) {
                        store.order(item);
                    }
                }

                ////////////////////////////////////////////////

                $scope.checkPurchase = function (item) {
                    //if (true) {
                    if ($scope.purchase[item].value == true) {
                        $scope.changeMenu(item);
                        $scope.getJson();
                    } else {
                        if (window.store) {
                            $scope.purchase[item].dirtyBit = true;
                            $scope.startPurchase(item);
                        } else {
                            $cordovaDialogs.alert('Unable to connect to Playstore', 'Purchase Item', 'Ok');
                        }
                    }
                }

                $scope.refreshPurchases = function () {
                    if ($scope.purchase['5startips'].value == false || $scope.purchase['Horse Racing Tips'].value == false || $scope.purchase['NFL Tips'].value == false || $scope.purchase['Boxing Tips'].value == false || $scope.purchase['Cricket Tips'].value == false || $scope.purchase['Tennis Tips'].value == false || $scope.purchase['Basketball Tips'].value == false || $scope.purchase['Baseball Tips'].value == false || $scope.purchase['NHL Tips'].value == false || $scope.purchase['Motor Racing Tips'].value == false || $scope.purchase['Golf Tips'].value == false) {
                        restoredPurchase.restoreClicked = true;
                        restoredPurchase.restored == false;
                        $cordovaDialogs.confirm('Your purchases will be restored shortly', 'Restore Purchase', ['Ok'])

                        store.refresh();

                    }

                    else {

                        $cordovaDialogs.confirm('Your purchases have already been restored', 'Restore Purchase', ['Ok'])

                    }
                }

                var restoring = function () {
                    if (restoredPurchase.restoreClicked == true && restoredPurchase.restored == false) {
                        restoredPurchase.restored = true;
                        $cordovaDialogs.confirm('Your previously purchased products have been restored', 'Purchases restored', ['Thanks!'])
                    }
                }

            }
            else {
                console.log("no store")
            }
        }

    }

    var getBadges = function () {
        if ($cordovaNetwork.getNetwork() == Connection.NONE) {
            $cordovaDialogs.alert(wifiAlert.message, wifiAlert.title, wifiAlert.button)
            $scope.badges = "";
        } else {
            apiService.fetchData(endPointGeneratorService.getEndPoint("Badges"))
                .then(function (data) {
                    if (data.success == 1) {
                        localStorage.setItem("totalTipsToday", data.stock[0]['TodayTotal']);
                        $scope.badges = data.stock[0];
                        $scope.newsFeed = data.newsFeed;
                        $scope.badges.FootballTips = parseInt($scope.badges.WeekTotal);
                    }
                })
                .catch(function (err, status) {
                    $scope.badges = "";
                    console.log(JSON.stringify(err, null, 4));
                });
        }
    }

    $scope.getAccuracy = function () {
        $scope.scrollTop();

        if ($cordovaNetwork.isOnline() == false) {
            $cordovaDialogs.alert(wifiAlert.message, wifiAlert.title, wifiAlert.button)
            $scope.internet = false;
        } else {
            $scope.internet = true;
            $ionicLoading.show({
                template: 'Loading...'
            });

            apiService.fetchData(endPointGeneratorService.getEndPoint("Accuracy"))
                .then(function (data) {
                    if (data.success == 1) {

                        for (var property in data.stock[0]) {
                            if (data.stock[0].hasOwnProperty(property)) {
                                $scope.accuracy[property].percent = data.stock[0][property]
                            }
                        }
                    }
                })
                .catch(function (err, status) {
                    console.log(JSON.stringify(err, null, 4));
                    showUnreachableServerAlert(function () { $scope.getAccuracy(); });
                })
                .finally(function () {
                    $ionicLoading.hide();
                })
        }
    }

    $scope.chooseLeagueStatsFunc = function (item) {
        if ($scope.chooseLeagueStatsBool == true) {
            if (typeof (item) == 'object') {
                if ($scope.stats[0].hasOwnProperty('League')) {
                    $scope.statsLeague = item.League;
                }
            }
            if (cordova.plugins.Keyboard.isVisible) {
                cordova.plugins.Keyboard.close();
            }
        }
        $scope.leagueSearch = '';
        $scope.chooseLeagueStatsBool = !$scope.chooseLeagueStatsBool;
    }

    $scope.getStats = function (statsType) {
        if ($cordovaNetwork.isOnline() == false) {
            $cordovaDialogs.alert(wifiAlert.message, wifiAlert.title, wifiAlert.button)
            $scope.internet = false;
        } else {
            $scope.internet = true;
            $scope.urlArgs.statsType = statsType;

            $ionicLoading.show({
                template: 'Loading...'
            });

            apiService.fetchData(endPointGeneratorService.getEndPoint("Stats-" + [$scope.urlArgs.statsType]))
                .then(function (data) {
                    if (data.success == 1) {
                        $scope.stats = data.stock;
                    }
                })
                .catch(function (err, status) {
                    console.log(JSON.stringify(err, null, 4));
                    showUnreachableServerAlert(function () { $scope.getStats(statsType); });
                })
                .finally(function () {
                    $ionicLoading.hide();
                    $scope.scrollTop();
                })
        }
    }


    $scope.changeNotifyTime = function (type) {
        switch (type) {
            case 'plusH':
                if ($scope.notifyTime.time.getHours() == 23) $scope.notifyTime.time.setHours(0);
                else $scope.notifyTime.time.setHours($scope.notifyTime.time.getHours() + 1)

                break;
            case 'minusH':
                if ($scope.notifyTime.time.getHours() == 0) $scope.notifyTime.time.setHours(23);
                else $scope.notifyTime.time.setHours($scope.notifyTime.time.getHours() - 1);
                break;
            case 'plusM':
                if ($scope.notifyTime.time.getMinutes() == 59) {
                    $scope.notifyTime.time.setMinutes(0);
                    $scope.changeNotifyTime('plusH')
                }
                else $scope.notifyTime.time.setMinutes($scope.notifyTime.time.getMinutes() + 1);
                break;
            case 'minusM':
                if ($scope.notifyTime.time.getMinutes() == 0) {
                    $scope.notifyTime.time.setMinutes(59);
                    $scope.changeNotifyTime('minusH')
                }
                else $scope.notifyTime.time.setMinutes($scope.notifyTime.time.getMinutes() - 1);
                break;
        }
    }

    function getNonFootballTips() {
        apiService.fetchData(endPointGeneratorService.getEndPoint($scope.urlArgs['first']))
            .then(function (data) {
                if (data.success == 1) {
                    $scope.nonFootballTips = data.stock;
                }
            })
            .catch(function (err, status) {
                $scope.nonFootballTips = [];
                showUnreachableServerAlert(function () { $scope.getJson(); });
            })
            .finally(function () {
                $ionicLoading.hide();
            });
    }

    function getFootballTips(showAlertIfUnreachable) {

        resetFootballTips();
        var endPoints = [];
        var promises = [];
        datesList = [];
        $scope.loading.football = true;

        if ($scope.purchase['5startips'].value == true) {
            endPoints = endPointGeneratorService.getAllFootballEndpoints();
        }
        else {
            endPoints = endPointGeneratorService.get4StarFootballEndpoints();
        }

        for (star in endPoints) {
            for (winType in endPoints[star]) {
                promises.push(sendRequest(star, winType));
            }
        }

        $q.all(promises)
            .then(function () {
                $ionicLoading.hide();

                // If key is "date", then it groups the arr array by date and returns an object with "date" keys
                var groupBy = function (arr, key) {
                    return arr.reduce(function (groupedObjWIP, arrItem) {
                        (groupedObjWIP[arrItem[key]] = groupedObjWIP[arrItem[key]] || []).push(arrItem);
                        return groupedObjWIP;
                    }, {});
                };

                var groupedTips = groupBy(getConcatedTipsArr(), "DateOnly");
                datesList = [];
                for (key in groupedTips) {
                    var dateListItem = {
                        alias: getDateAlias(key),
                        date: new Date(key),
                    }

                    datesList.push(dateListItem)
                }

                datesList.sort(function (a, b) {
                    // Turn your strings into dates, and then subtract them
                    // to get a value that is either negative, positive, or zero.
                    return new Date(a.date) - new Date(b.date);
                });

                var indexOfToday = datesList.map(function (d) { return d.alias; }).indexOf('Today');
                scrollableDate_centerIndex = indexOfToday > -1 ? indexOfToday : 0;

                setScrollableDatesScroll();
            })
            .catch(function () {
                if (showAlertIfUnreachable == true) {
                    showUnreachableServerAlert(function () { $scope.getJson(); });
                }
            })
            .finally(function () {
                $scope.loading.football = false;
            })

        function getDateAlias(key) {
            var momentDateToCheck = moment(key);
            var TODAY = moment().startOf('day');;
            var YESTERDAY = moment().subtract(1, 'days').startOf('day');;
            var TOMORROW = moment().add(1, 'days').startOf('day');;

            if (momentDateToCheck.isSame(TODAY)) return 'Today';
            if (momentDateToCheck.isSame(YESTERDAY)) return 'Yesterday';
            if (momentDateToCheck.isSame(TOMORROW)) return 'Tomorrow';

            return moment(key).format('ddd, MMM DD');
        }

        function sendRequest(star, winType) {
            return $q(function (resolve, reject) {
                apiService.fetchData(endPointGeneratorService.getEndPoint('Football Tips-' + star + ' Star-' + winType))
                    .then(function (data) {
                        if (data.success == 0) return;
                        data.stock.forEach(function (tip) {
                            var tempDate = new Date(tip.Date.replace(' ', 'T')); // the date format returned by the api returns null if used with Date() in ios. This replace statement fixes the format
                            tempDate.setHours(0, 0, 0, 0);
                            tip.DateOnly = tempDate;
                        }, this);

                        $scope.footballTips["" + winType]["tips"]["" + star] = data.stock;
                    })
                    .catch(function (err, status) {
                        console.log(JSON.stringify(err, null, 4));
                        reject();
                    })
                    .finally(function () {
                        resolve();
                    });
            });

        }
    }

    function getConcatedTipsArr(chosenWinType) {
        var concatedArr = [];
        $scope.winType_list.forEach(function (winType) {
            if (chosenWinType != null && winType != chosenWinType) return;
            concatedArr = concatedArr.concat($scope.footballTips["" + winType]['tips']['4']);
            concatedArr = concatedArr.concat($scope.footballTips["" + winType]['tips']['5']);
        }, this);
        return concatedArr;
    }

    function resetFootballTips() {
        var index = 0;
        $scope.winType_list.forEach(function (winType) {
            $scope.footballTips["" + winType] = {
                "tips": {
                    "5": [],
                    "4": []
                },
                "isOpen": true,
            }
        }, this);
    }

    $scope.getJson = function () {

        $scope.nonFootballTips = [""];

        if ($cordovaNetwork.isOnline() == false) {
            $cordovaDialogs.alert(wifiAlert.message, wifiAlert.title, wifiAlert.button)
            $scope.internet = false;
            return;
        }

        $scope.internet = true;
        $ionicLoading.show({
            template: 'Loading...'
        });

        if ($scope.urlArgs['first'] == 'Football Tips') getFootballTips();
        else getNonFootballTips();
    }



    $scope.shareMessage = function () {
        var message = "I’m using CG Tipster for my bets, get it here\n\n"
        if ($scope.devicePlatform == 'iOS')
            message += "https://itunes.apple.com/us/app/cgtipster-sports-tips-stats/id1019083868?ls=1&mt=8"
        else
            message += "http://play.google.com/store/apps/details?id=com.bigedev.cgtipster"

        $cordovaSocialSharing
            .share(message, 'cgTipster Tips') // Share via native share sheet
            .then(function (result) {
            }, function (err) {
                console.log("error" + JSON.stringify(err, null, 4))
            })
    }

    var getLeagues = function () {

        if ($cordovaNetwork.isOnline() == false) {
            $scope.internet = false;
            $cordovaDialogs.alert(wifiAlert.message, wifiAlert.title, wifiAlert.button)
            $scope.leagues = []
            $scope.selectedLeagues = []
            $scope.changeMenu();
        } else {
            $scope.internet = true;
            $ionicLoading.show({
                template: 'Loading...'
            });

            apiService.fetchData(endPointGeneratorService.getEndPoint("Leagues"))
                .then(function (data) {
                    if (data.success == 1) {
                        $scope.leagues = data.stock;
                        $scope.setLeaguesSame(false);

                        $scope.leagues.forEach(function (leagueObj) {
                            $scope.allLeaguesArray.push(leagueObj.League)
                        })
                        $scope.selectedLeagues = $scope.allLeaguesArray;
                        $scope.getJson();
                    }
                })
                .catch(function (err, status) {
                    $scope.leagues = [];
                    console.log(JSON.stringify(err, null, 4));
                    $ionicLoading.hide();

                    showUnreachableServerAlert(function () {
                        getBadges();
                        getLeagues();
                    });
                })
                .finally(function () {

                })
        }
    }

    $scope.setLeaguesSame = function (value) {

        $scope.leagues.forEach(function (leagueObj) {
            leagueObj.value = value;
        })
        if (value == true) {
            $scope.selectAllLeagues = true;
        } else {
            $scope.selectAllLeagues = false;
        }
    }

    $scope.changeMenu = function (chosenMenu) {
        for (var property in $scope.sideMenuOptions) {
            if ($scope.sideMenuOptions.hasOwnProperty(property)) {
                $scope.sideMenuOptions[property] = false;
            }
        }

        $scope.sideMenuOptions[chosenMenu] = true;

        $scope.urlArgs['first'] = chosenMenu;
        if (chosenMenu == 'Football Tips') {
            $scope.urlArgs.star = '4 Star';
        }
        else if (chosenMenu == 'Stats') {
            $scope.urlArgs.statsType = 'Main Table'
        }
        toggleLeft(false);
    }

    var toggleLeft = function (arg) {
        if (arg != null) {
            $ionicSideMenuDelegate.toggleLeft(arg);
        } else {
            $ionicSideMenuDelegate.toggleLeft();
        }
    };

    $scope.goBackFromChooseLeague = function () {
        $scope.leagueSearch.value = ''
        $scope.leagueValues.trueValue = 0;
        $scope.leagueValues.falseValue = 0;
        $scope.selectedLeagues = [];

        $scope.leagues.forEach(function (leagueObj) {
            if (leagueObj.value == true) {
                $scope.leagueValues.trueValue++;
                $scope.selectedLeagues.push(leagueObj.League);
            }
            else {
                $scope.leagueValues.falseValue++;
            }
        })

        if ($scope.leagueValues.falseValue == $scope.leagues.length) {
            $scope.selectedLeagues = $scope.allLeaguesArray;
        }

        $scope.changeMenu('Football Tips');
        $scope.getJson();
        if (cordova.plugins.Keyboard.isVisible) {
            cordova.plugins.Keyboard.close();
        }
    }

    $scope.openPopup = function (popType) {
        $scope.popType = popType;

        if (popType == 'timePicker') {
            toggleLeft(false);
            $scope.notifyTime.time = new Date();
        }

        popUp = $ionicPopup.show({
            templateUrl: "templates/" + popType + ".html",
            scope: $scope
        });

    }

    $scope.closePopup = function (itemType) {
        if ($scope.popType == 'timePicker') {
            if (itemType == 'Disable') {
                toggleNotification(false);
            }
            else if (itemType == 'Notify') {
                setNotificationTime();
            }
            popUp.close();
        }
        else if ($scope.popType == 'popWin') {
            $scope.urlArgs.winType = itemType;
            $scope.getJson();
            popUp.close();
        }
    }

    $scope.starSelect = function (itemType) {
        if (itemType == '5 Star') {
            if ($scope.purchase['5startips'].value == true) {
                $scope.urlArgs.star = itemType;
            } else {
                $scope.purchase['5startips'].dirtyBit = true;
                $scope.startPurchase('5startips');
            }
        } else {
            $scope.urlArgs.star = itemType;
        }
    }

    function createSelectedBanner() {
        if (
            $scope.purchase['5startips'].value == false ||
            $scope.purchase['Horse Racing Tips'].value == false ||
            $scope.purchase['NFL Tips'].value == false ||
            $scope.purchase['Boxing Tips'].value == false ||
            $scope.purchase['Cricket Tips'].value == false ||
            $scope.purchase['Tennis Tips'].value == false ||
            $scope.purchase['Basketball Tips'].value == false ||
            $scope.purchase['Baseball Tips'].value == false ||
            $scope.purchase['NHL Tips'].value == false ||
            $scope.purchase['Motor Racing Tips'].value == false ||
            $scope.purchase['Golf Tips'].value == false) {

            admobService.createAds();

        }

    }

    $scope.mailUs = function () {
        toggleLeft(false);
        if ($scope.devicePlatform == 'iOS')
            window.open('mailto:info@cgtipster.com', 'mail');
        else
            window.open('mailto:info@cgtipster.com', '_system');








    }

    $scope.rateUs = function () {
        toggleLeft(false);
        var string = 'en';
        AppRate.preferences.customLocale = AppRate.locales.getLocale(navigator.language);
        AppRate.preferences.storeAppURL.android = 'market://details?id=com.bigedev.cgtipster';
        AppRate.preferences.storeAppURL.ios = '1019083868';
        AppRate.promptForRating(true);
    }

    //// Has TO SHOW ///////////////////

    $ionicPlatform.ready(function () {
        ////    ////// HAS TO SHOW //////////////////////////////////
        var notificationMsg = localStorage.getItem('totalTipsToday')

        document.addEventListener("online", function () {
            console.log("online")
            if (onlineOnStart == false) {
                onlineOnStart = true;
                firstOnLine();


                $cordovaDialogs.confirm('You are now connected to the internet', 'Ready for Tips', [wifiAlert.button])

            } else {
                getBadges();
            }
        }, false);

        if ($cordovaNetwork.isOnline() == true) {


            firstOnLine();

        } else {
            onlineOnStart = false;

            $cordovaDialogs.confirm(wifiAlert.message, wifiAlert.title, [wifiAlert.button])
                .then(function (buttonIndex) {
                    ionic.Platform.exitApp();
                });
        }
    });
    ////////////?????????????????????????//////////////////////????//////////

    ////////////notifications DOWN ///////////////////////////////////////////

    var firstOnLine = function () {

        setTimeout(function () {
            if ($scope.purchase['5startips'].value == false) {
                $scope.delay5starPurchaseBtn = true;
                $timeout(function () {
                    //$digest or $apply
                    $scope.$apply();
                })
            }
        }, 3000)

        if (localStorage.getItem('notifyHour') == null) {
            localStorage.setItem('notifyHour', '9');
            localStorage.setItem('notifyMinute', '0');
        }
        if (localStorage.getItem('notificationEnabled') === false) {
            toggleNotification('false');
        } else {
            toggleNotification('true');
        }

        createSelectedBanner();
        initialize();

    }

    var toggleNotification = function (setNotify) {
        localStorage.setItem('notificationEnabled', setNotify)
        if (setNotify == 'true') {
        } else {
            cordova.plugins.notification.local.cancelAll(function () {
                $cordovaToast.showShortBottom("Notifications have been disabled")
            });
        }
    }

    $scope.notifyTap = function () {
        navigator.notification.confirm("Do you want to receive daily notifications from CGTipster?", onNotifyConfirm, "Notify Me", ['Yes', 'No'])

    }

    var onNotifyConfirm = function (buttonIndex) {
        if (buttonIndex == 1) {
            toggleNotification('true');
        } else if (buttonIndex == 2) {
            toggleNotification('false');
        }
    }

    var setNotifications = function (timeSetup, notificationText) {
        var setOptions = {
            id: 200,
            text: notificationText,
            firstAt: timeSetup,
            every: 'day'
        }

        cordova.plugins.notification.local.cancelAll(function () {
            cordova.plugins.notification.local.schedule(setOptions);
        });
    }

    var setNotificationTime = function () {

        localStorage.setItem('notifyHour', $scope.notifyTime.time.getHours().toString());
        localStorage.setItem('notifyMinute', $scope.notifyTime.time.getMinutes().toString());
        setNotifications(setTime(), defaultNotificationText)
        $cordovaToast.showShortBottom("Notification time has been set")
    }

    var setTime = function () {
        var currentTime = new Date();
        var hrs = parseInt(localStorage.getItem('notifyHour'));
        var mint = parseInt(localStorage.getItem('notifyMinute'));

        var newNotifyTime = new Date();
        newNotifyTime.setHours(hrs);
        newNotifyTime.setMinutes(mint);
        newNotifyTime.setSeconds(5);

        if ((hrs < currentTime.getHours()) || ((hrs == currentTime.getHours()) && (mint < currentTime.getMinutes()))) {
            newNotifyTime.setDate(currentTime.getDate() + 1);
        }
        return newNotifyTime;
    }
    ///////////////// notifications UP ////////////////////////////////////
    $scope.scrollTop = function () {
        $ionicScrollDelegate.scrollTop();
    };

    var isScrollTimeout_right = true;
    var isScrollTimeout_left = true;
    $scope.scrollableDate_onSwipeRight = function () {
        $ionicScrollDelegate.getScrollView().options.scrollingY = false;
        if (isScrollTimeout_right == true) {
            if (scrollableDate_centerIndex != 0) {
                scrollableDate_centerIndex--;
                setScrollableDatesScroll();
                $ionicScrollDelegate.scrollTop(true);
            }
            isScrollTimeout_right = false;
            setTimeout(function () {
                $ionicScrollDelegate.getScrollView().options.scrollingY = true;
                isScrollTimeout_right = true;

                $timeout(function () {
                    $scope.$apply();
                });
            }, 90);
        }
    }

    $scope.scrollableDate_onSwipeLeft = function () {
        $ionicScrollDelegate.getScrollView().options.scrollingY = false;
        if (isScrollTimeout_left == true) {
            if (scrollableDate_centerIndex != datesList.length - 1) {
                scrollableDate_centerIndex++;
                setScrollableDatesScroll();
                $ionicScrollDelegate.scrollTop(true);
            }
            isScrollTimeout_left = false;
            setTimeout(function () {
                $ionicScrollDelegate.getScrollView().options.scrollingY = true;
                isScrollTimeout_left = true;

                $timeout(function () {
                    $scope.$apply();
                });
            }, 90);
        }
    }

    $scope.onPurchaseChanged = function () {
        if (
            $scope.urlArgs.first == 'Football Tips' &&
            $scope.purchase['5startips'].value == true) {

            $timeout(function () {
                $scope.$apply(function () {
                    getFootballTips(false);
                })
            })

        }
    }

    function showUnreachableServerAlert(callback) {
        navigator.notification.alert("Unable to reach CGTipster at this time. Please try again later",
            callback,
            "Connection failed", "Try Again");
    }

    $scope.toggleTipTypeCard = function (winTypeCard) {
        winTypeCard.isOpen = !winTypeCard.isOpen;
        $ionicScrollDelegate.resize();
    }

    $scope.shouldShow = function (tip) {
        if (tip == null || $scope.scrollableDate_center == null) return;
        var selectedDate = moment($scope.scrollableDate_center.date).startOf('day');
        return selectedDate.isSame(moment(tip.DateOnly)) == true;
    }

    $scope.checkTipsAvailability = function (winType) {
        if ($scope.footballTips[winType]['tips']['4'].length + $scope.footballTips[winType]['tips']['5'].length == 0) return false;



        var flag = false;
        getConcatedTipsArr(winType).forEach(function (tip) {
            if ($scope.shouldShow(tip) == true) {
                flag = true;
                return;
            }
        }, this);
        return flag;
    }

    $scope.$watch("isPurchasedGold", $scope.onPurchaseChanged, true);
});