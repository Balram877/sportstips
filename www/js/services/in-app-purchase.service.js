angular.module('tipster')
    .factory('inAppPurchaseService', inAppPurchaseService);

inAppPurchaseService.$inject = [];

function inAppPurchaseService() {

    return {
        initializeStore: initializeStore
    }

    function initializeStore() {

        _registerItems();

        store.refresh();

    }

    function _registerItems() {
        store.register({
            id: "5startips",
            alias: "5startips",
            type: store.NON_CONSUMABLE
        });

        store.register({
            id: "horseracingtips",
            alias: "Horse Racing Tips",
            type: store.NON_CONSUMABLE
        });

        store.register({
            id: "nfltips",
            alias: "NFL Tips",
            type: store.NON_CONSUMABLE
        });

        store.register({
            id: "boxingtips",
            alias: "Boxing Tips",
            type: store.NON_CONSUMABLE
        });

        store.register({
            id: "basketballtips",
            alias: "Basketball Tips",
            type: store.NON_CONSUMABLE
        });

        store.register({
            id: "nhltips",
            alias: "NHL Tips",
            type: store.NON_CONSUMABLE
        });

        store.register({
            id: "golftips",
            alias: "Golf Tips",
            type: store.NON_CONSUMABLE
        });

        store.register({
            id: "crickettips",
            alias: "Cricket Tips",
            type: store.NON_CONSUMABLE
        });

        store.register({
            id: "tennistips",
            alias: "Tennis Tips",
            type: store.NON_CONSUMABLE
        });

        store.register({
            id: "entireapp",
            alias: "entireapp",
            type: store.NON_CONSUMABLE
        });

        store.ready(function () {
            console.log("\\o/ STORE READY \\o/ " + store.products.length);
        });
    }

}
