angular.module('tipster')
  .factory('endPointGeneratorService', endPointGeneratorService);

endPointGeneratorService.$inject = [];

function endPointGeneratorService() {

  var testVar = 0;

  var URLs = {
    baseURL: 'http://api.cgtipster.com/',
    endPoints: {

      'Football Tips': {
        '4 Star': {
          'Home Wins': 'homeFour',
          'Away Wins': 'awayFour',
          'Over 1.5': 'highFour',
          'Over 2.5': 'overTwoFour',
          'Under 2.5': 'lowFour',
          'BTTS': 'bttsFour',
        },
        '5 Star': {
          'Home Wins': 'homeFive',
          'Away Wins': 'awayFive',
          'Over 1.5': 'highFive',
          'Over 2.5': 'overTwoFive',
          'Under 2.5': 'lowFive',
          'BTTS': 'bttsFive',
        }
      },

      'Horse Racing Tips': 'horseRacing',
      'Tennis Tips': 'tennis',
      'Cricket Tips': 'cricket',
      'NFL Tips': 'nfl',
      'Boxing Tips': 'boxing',
      'Basketball Tips': 'basketball',
      'NHL Tips': 'nhl',
      'Golf Tips': 'golf',

      'Accuracy': 'accuracy',
      'Leagues': 'league',
      'Badges': 'total',

      'Stats': {
        'Main Table': 'maintable',
        'Home Table': 'hometable',
        'Away Table': 'awaytable',
        'Last 5': 'lastfive',
        'Last 5 Home': 'lastfivehome',
        'Last 5 Away': 'lastfiveaway',
      },

    }
  }

  return {
    getEndPoint: getEndPoint,
    getAllFootballEndpoints: getAllFootballEndpoints,
    get4StarFootballEndpoints: get4StarFootballEndpoints,
    getAllWinTypes: getAllWinTypes
  }

  function getEndPoint(type) {
    var endPointBase = URLs.baseURL;

    var typeArr = type.split("-");
    var endPointPath = URLs.endPoints;
    typeArr.forEach(function (element) {
      endPointPath = endPointPath[element];
    }, this);

    // **** Uncomment TO test Unreachable server ****
    // if (!type.toLowerCase().startsWith("badges") && testVar++ % 6 == 0) {
    //   endPointBase = endPointBase.replace(".com", ".com:81");
    //   console.log(endPointBase + "" + endPointPath + " ... next testVar % 6 = " + testVar % 6);
    // } else {
    //   console.log(endPointBase + "" + endPointPath + " ... next testVar % 6 = " + testVar % 6);
    // }
    // **********************************************

    return endPointBase + "" + endPointPath;
  }

  function getAllFootballEndpoints() {
    return {
      '4': URLs.endPoints['Football Tips']['4 Star'],
      '5': URLs.endPoints['Football Tips']['5 Star'],
    };
  }

  function get4StarFootballEndpoints() {
    return {
      '4': URLs.endPoints['Football Tips']['4 Star'],
    };
  }

  function getAllWinTypes() {
    var winTypes = [];
    for (key in URLs.endPoints['Football Tips']['4 Star']) {
      winTypes.push(key);
    }
    return winTypes;
  }

}
