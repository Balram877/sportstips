angular.module('tipster')
    .factory('admobService', admobService);

admobService.$inject = [];

function admobService() {

    var admobid = {};
    if (/(android)/i.test(navigator.userAgent)) {
        admobid = { // for Android
            banner: 'ca-app-pub-5631685820346651/4552809928',
            interstitial: 'ca-app-pub-6869992474017983/1657046752'
        };
    } else if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
        admobid = { // for iOS
            banner: 'ca-app-pub-5631685820346651/8890010721',
            interstitial: 'ca-app-pub-6869992474017983/7563979554'
        };
    } else {
        admobid = { // for Windows Phone
            banner: 'ca-app-pub-6869992474017983/8878394753',
            interstitial: 'ca-app-pub-6869992474017983/1355127956'
        };
    }

    var banner = {
        'created': false,
        'removed': false
    }

    return {
        createAds: createAds,
        removeAds: removeAds
    }

    function createAds() {
        AdMob.createBanner(
            {
                adId: admobid.banner,
                position: AdMob.AD_POSITION.BOTTOM_CENTER,
                overlap: false,
                adSize: 'SMART_BANNER',
                isTesting: false,
                autoShow: true
            },
            function (success) {
                banner.created = true;
                banner.removed = false;

            }, function (failure) {
                console.log('inside create banner 2 ff');
            }
        );
    }

    function removeAds() {
        if (banner.created == true) {
            AdMob.removeBanner();
            banner.created = false;
            banner.removed = true;
        }
    }

}
