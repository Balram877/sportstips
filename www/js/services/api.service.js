app.factory('apiService', apiService);

apiService.$inject = ['$q', '$http'];

function apiService($q, $http) {

    return {
        fetchData: fetchData
    }

    function fetchData(url) {
        return $q(function (resolve, reject) {
            $http.get(url, { timeout: 20000 })
                .then(
                function (data) {
                    resolve(data.data);
                },
                function (err, status) {
                    reject(err, status)
                });
        });
    }
}